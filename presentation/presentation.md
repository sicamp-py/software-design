% Проектирование
% Анкудинов Александр
% Python.ФИИТ 2021

# Ещё чуток про чистый код

"Пишите код, за который вам не будет стыдно." - первый пункт одного из реальных Coding Style Guide ;)


# Disclaimer

* hot topic
* рассказываю во многом опираясь на свой опыт в Яндексе/разработке и админстве для разных соревнований
* который сколько-то совпадает с опытом [других](https://blog.pragmaticengineer.com/software-architecture-is-overrated/)
* реальные примеры приводить сложно (для них нужен большой контекст)

# Что такое проектирование

С просторов интернета:

> Software design is a process to transform user requirements into some suitable form, which helps the programmer in software coding and implementation.


Из SWEBOK Guide v3:

> Design is defined as both “the process of defining the architecture, components, interfaces,
> and other characteristics of a system or component” and “the result of that process”


P.S: SWEBOK = Software Engineering Body of Knowledge


# Как это происходит в жизни?

1. сбор и анализ требований
2. исследования (в том числе возможно PoC'и прототипы)
3. продумываем то, как мы будем реализовывать
4. защищаем полученный дизайн
5. намечаем план, нарезаем задачи, оцениванием
6. делаем их, корректируя план по ходу дела, донарезая забытое
7. выкатываем в PROD

# 1. Сбор и анализ требований

* Какую задачу мы пытаемся решить?
* Что за продукт мы строим, почему?
* Как мы поймём, что у нас получилось? Как измерим успех?

\pause

Задача: хотим автоматически проверять задачку про VIM

* пользователям даётся два текста и задание за меньше чем N нажатий преобразовать один в другой
* они присылают нам последовательность нажатий клавиш, нужно попробовать понять, сработает ли она
* нужно держать нагрузку в 100-200 пользователей одновременно

# VIM

[Ссылка на условие](https://gist.github.com/xelez/3f10ac062bfecb6d67158b0998329361)

![Кин-дза-дза выходят из VIM](img/howto-exit-vim.jpeg){ width=50% }


# 1. Сбор и анализ требований

* Какую задачу мы пытаемся решить?
* Что за продукт мы строим, почему?
* Как мы поймём, что у нас получилось? Как измерим успех?

\pause

Продукт? Система проверки

Почему? Это весело :D

\pause

Измерим успех? Задача отработала на "Неконтесте", для чего надо бы уметь на всякий случай проверять задачки у 100-200 пользователей одновременно.

# 2. Исследование

Пытаемся понять, как это вообще можно написать.

1. Ищем, можем ли мы найти какой-то эмулятор vim'а и встроить его себе?
2. Может мы можем придумать как запустить VIM и скормить ему нажатия клавишь?
3. Как запустить его безопасно.

\pause

Делаем прототип из python+docker+tmux+vim, убеждаемся что так в целом будет работать.

# 2. Исследование

Проектирование - это про принятие решений или их откладывание/возможность изменить их.

Пример принятия решений:

 * разделение задачи на независимые сервисы

Пример откладывания решения:

 * абстрагируемся от используемой БД

# 3. Продумываем реализацию

Будем честны, в данном случае я просто нарисовал на бумажке что-то подобное)

![Моя ужасная схемка](img/vimulator-arch.png)

Обычно результат: набор схем и документов на wiki.

# 3. Продумываем реализацию


Архитектура Contrail: системы для создания виртуальных сетей.

![https://juniper.github.io/contrail-vnc/architecture.html](img/contrail-view.jpg){ width=60% }


P.S. Ныне проект называется TungstenFabric

# 4. Защищаем дизайн

В реальности: одна или несколько встреч на примерно час.

\pause

Рассказать:

* что?
* зачем?
* как будем делать?
* почему именно так?
* ответить на вопросы коллег

\pause

Записать не отвеченные и уйти править недостатки, и готовится отвечать на оставшиеся.

# 5. Намечаем план, нарезаем задачи

* сделать скелет на flask
* сделать схему для DB
* реализовать web-интерфейс
  * сделать html со страницей отправки
  * хендлер отправки решения
  * хендлер для получения его статуса
* сделать docker-контейнер c vim+tmux
* реализовать воркер
  * обработку очереди
  * валидацию и разбор отправленных нажатий
  * запуск vim'а
  * сохранение результата посылки
  * сделать этот запуск безопасным

# 6 и 7. Осталось лишь написать и выкатить

<вставить мем>


# Что самое важное при дизайне?

* понимание предметной области
* здравый смысл =)

# Что должно получится в результате хорошего дизайна?

* систему легко тестировать
* при изменении требований/исправлений ошибок нужно менять только в одном месте
* несколько человек может работать над проектом параллельно
* систему легко обновлять
* можно масштабировать под нагрузку


# Пример упрощения жизни

За счёт не самого плохого дизайна...


![TungstenFabric](img/tf-arch.png)

# Как учиться дизайнить системы?

![https://www.freecodecamp.org/news/software-design/](img/design-layers.png)

# Из самых важных идей

* SOLID
* KISS
* DRY
* YAGNI

P.S. biassed

# Вкратце про SOLID

* **S**ingle Responsibility Principle
* **O**pen-Closed principle
* **L**iskov Subsition Principle
* **I**nterface Segregation Principle
* **D**ependency Injection Principle

# SRP

Нарушает ли это SRP?

```python
class Animal:
    def __init__(self, name: str):
        self.name = name
    
    def get_name(self) -> str:
        pass

    def save(self, animal: Animal):
        #somehow saves it in DB
```

# SRP

Обычно пишут что да, и что выделить отдельный класс лучше

```python
class Animal:
    def __init__(self, name: str):
        self.name = name
    
    def get_name(self) -> str:
        pass

class AnimalDB:
    def get_animal(self, id) -> Animal:
        pass

    def save(self, animal: Animal):
        pass
```

# SRP

Только это (анти?)паттерн ActiveRecord, который используется например в Django!

```python
class Animal:
    def __init__(self, name: str):
        self.name = name
    
    def get_name(self) -> str:
        pass

    def save(self, animal: Animal):
        #somehow saves it in DB
```



# Более реальный пример нарушения SRP

```python
class MyGame:
  def create_ui(self):
     pass
     
  def handle_collision(self):
     pass
  
  def load_saved_game(self):
     pass

  def on_key_press(self):
     pass
     
```

# Ещё более реальный пример нарушения SRP

https://github.com/tungstenfabric/tf-controller/blob/dc50a1b8ac3ead4c45945ba6a52a831173ff5402/src/config/common/cfgm_common/vnc_amqp.py#L19

Этот класс:

* судя по названию что-то обрабатывает...
* управляет соединением.. наверное
* обрабатывает сообщения из очереди
* обрабатывает зависимые объекты из этих сообщений?
* делает ещё что-то связанное с трассировкой
* наверняка делает что-то ещё, что я упустил...


# Ещё более реальный пример нарушения SRP

Если нельзя нормально выразить за что отвечает класс - что-то пошло не так!

# Liskov substitution + interface segregation

\footnotesize

```python
class IFile:
    @abstract_method
	def read_line(self, line):
		pass
		
	@abstract_method
	def write_line(self, line):
		pass
		
class ReadOnlyFile(IFile):        
    def read_line(self, line):
        pass
		
	def write_line(self, line):
		raise NotImplementedError("File is read-only")
```


# Liskov substitution + interface segregation

\footnotesize

```python


class IReadable:
	@abstract_method
	def read_line(self, line):
		pass

class IWritable:
	@abstract_method
	def write_line(self, line):
		pass

class ReadOnlyFile(IReadable):
	def read_line(self, line):
		pass
		
```


# Duck typing

"If it walks like a duck and it quacks like a duck, then it must be a duck"

![В Python часто нет явных интерфейсов](img/duck-typing.jpg)


# Dependency injection

```python
class QueueHandler:
   def __init__(self, db_args):
       self._db = DbConnection(**db_args)
       
   def is_empty(self):
       self._db.some_query()
```


# Dependency injection

Better:

```python
class QueueHandler:
   def __init__(self, db_connection):
       self._db = db_connection
       
   def is_empty(self):
       self._db.some_query()
```


# Главное не перебарщивать

![Только ситхи всё возводят в абсолют](img/sith.png)

# И пользоваться здравым смыслом

![Хороший попугай](img/parrot.jpg)

Note: мем из профунктора

# Есть и другой способ

Смотреть на то, как устроены существующие проекты/библиотеки/фреймворки, в том числе внутри.


# Practice

* у вас есть таск
* давайте разберёмся как устроен TheFuck


# О чем подумать?

https://github.com/nvbn/thefuck/tree/master/thefuck

* Как в принципе работает эта штука? (можно попробовать нарисовать схемку)
* Как представляется "команда"?
* Как добавить новое правило исправления?
* Как бы попробовали добавить туда AI?


# Разбор


# Напоследок

Software architecture best practices, enterprise architecture patterns, and formalized ways to describe systems are all tools that are useful to know of and might come in handy one day. But **when designing systems, start simple and stay as simple as you can. Try to avoid the complexity that more complex architecture and formal tools inherently introduce.**



# Links, sources, etc.

* https://www.freecodecamp.org/news/software-design/
* https://blog.pragmaticengineer.com/software-architecture-is-overrated/


