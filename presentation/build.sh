#!/bin/bash
pandoc -t beamer presentation.md -o presentation.pdf --pdf-engine=xelatex --variable mainfont="DejaVu Serif" -V theme:Warsaw
